Package.describe({
  summary: 'Mediator for subscribing to named events',
  version: '1.1.0',
  name: 'jsgoyette:mediator'
});

Package.onUse(function (api) {

  api.use([
    'check',
    'underscore',
    'tracker'
  ]);

  api.addFiles('mediator.js');

  api.export('Mediator');

});

Package.onTest(function(api) {

  api.use([
    'jsgoyette:mediator',
    'tracker',
    'tinytest',
    'test-helpers'
  ]);

  api.addFiles('tests/empty.js');
  api.addFiles('tests/unit.js');

});
