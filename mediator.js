Mediator = {

  channels: {},

  publish: function(name) {

    // abandon if channel does not exist
    if (!this.channels[name]) return false;

    // collect args and fire changed
    this.channels[name].args = _.toArray(arguments).splice(1);
    this.channels[name].deps.changed();
    return true;
  },

  subscribe: function(name) {

    // force channel name to string or number
    check(name, Match.OneOf(String, Number));

    // create channel if new
    if (!this.channels[name]) {
      this.channels[name] = {
        deps: new Tracker.Dependency,
        args: null
      };
    }

    // call depend and return args
    this.channels[name].deps.depend();
    return this.channels[name].args;
  }
};
