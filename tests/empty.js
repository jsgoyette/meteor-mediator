Tinytest.add('Mediator Empty - publish with no channel name should do nothing', function (test) {
  Mediator.publish();
});

Tinytest.add('Mediator Empty - publish to empty subscription should do nothing', function (test) {
  Mediator.publish('chsldkncku', 'testing');
});
