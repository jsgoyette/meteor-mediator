var affected = false;

Meteor.startup(function() {
  return Tracker.autorun(function() {
    affected = Mediator.subscribe('test');
  });
});

Tinytest.add('Mediator Unit - publication with no args should trigger subscription with empty array', function (test) {

  Mediator.publish('test');
  Tracker.flush();
  test.equal(affected, []);

});

Tinytest.add('Mediator Unit - publication with args should trigger subscription with array of values', function (test) {

  Mediator.publish('test', 'abc');
  Tracker.flush();
  test.equal(affected, ['abc']);

});
