# Mediator

Mediator is a simple package for managing events. It is intended to be used to facilitate event between packages, though it can also be used for events within a single package.

Based on [article by Manuel Schoebel](http://www.manuel-schoebel.com/blog/meteorjs-package-only-app-structure-with-mediator-pattern).

## Usage
To use, create a new subscription channel by calling subscribe with an autorun. The subscribe method takes one argument, which is the channel's name.

```js
Meteor.startup(function() {
  return Tracker.autorun(function() {
    var args = Mediator.subscribe('test');
    // do something
    // console.log(args);
  });
});
```

To publish data to the channel, simply call publish with the channel name as the first argument. Subsequent arguments will get passed as arguments to the subscription.

```js
var user_id = 'xyz';

Mediator.publish('test', user_id, {
  a: 123,
  b: 'cat'
});
```
